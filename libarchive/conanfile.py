#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile


class LibArchive(ConanFile):
    name = "libarchive"
    version = "3.3.3"
    license = "BSD-2-Clause"
    url = "https://bitbucket.org/headcount_bimandscan/conan-libarchive"
    description = "A multi-format archive and compression library."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://www.libarchive.org"

    _src_dir = f"{name}-{version}"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    exports = "../LICENCE.md"

    requires = "libiconv/1.15@bincrafters/stable", \
               "pcre2/10.31@bincrafters/stable", \
               "lzma/5.2.4@bincrafters/stable", \
               "zstd/1.3.5@bincrafters/stable", \
               "bzip2/1.0.6@conan/stable", \
               "zlib/1.2.11@conan/stable", \
               "libxml2/2.9.9@bincrafters/stable", \
               "lz4/1.8.3@bincrafters/stable", \

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        zip_name = f"{self._src_dir}.zip"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"
        src_cmakefile2 = f"{self._src_dir}/libarchive/CMakeLists.txt"

        tools.download(f"https://www.libarchive.org/downloads/{zip_name}",
                       zip_name)

        tools.unzip(zip_name)
        os.unlink(zip_name)

        # Patch CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "PROJECT(libarchive C)",
                              "PROJECT(libarchive C)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup()\n")

        # Only install static or shared libraries, never both:
        if self.options.shared:
            tools.replace_in_file(src_cmakefile2,
                                  "INSTALL(TARGETS archive archive_static",
                                  "install(TARGETS archive")
        else:
            tools.replace_in_file(src_cmakefile2,
                                  "INSTALL(TARGETS archive archive_static",
                                  "install(TARGETS archive_static")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["ENABLE_TEST"] = False
        cmake.definitions["ENABLE_COVERAGE"] = False
        cmake.definitions["ENABLE_INSTALL"] = True
        cmake.definitions["BUILD_TESTING"] = False
        cmake.definitions["ENABLE_TAR"] = False
        cmake.definitions["ENABLE_CAT"] = False
        cmake.definitions["ENABLE_CPIO"] = False
        cmake.definitions["ENABLE_EXPAT"] = False
        cmake.definitions["ENABLE_ACL"] = False

        # TODO(neil): allow crypto in future versions?
        cmake.definitions["ENABLE_NETTLE"] = False
        cmake.definitions["ENABLE_OPENSSL"] = False

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("COPYING",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")
