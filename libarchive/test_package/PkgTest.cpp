/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <archive.h>
#include <archive_entry.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'libArchive' package test (compilation, linking, and execution).\n";

    struct archive *ina;
	struct archive *outa;
	struct archive_entry *entry;

    std::cout << "'libArchive' package works!" << std::endl;
    return EXIT_SUCCESS;
}
