# BIM & Scan® Third-Party Library (libArchive)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [libArchive](https://www.libarchive.org/), a multi-format archive and compression library.

Supports version 3.3.3 (stable).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) Conan repository for third-party dependencies.
