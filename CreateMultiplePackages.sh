#!/bin/bash

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
set -e

CXX_STDS=("11"
          "14"
          "17"
          "20")

for CXX_STD in "${CXX_STDS[@]}"
do
    conan create --build="missing" -s compiler.cppstd="$CXX_STD" -o libarchive:shared=True "$@" "libarchive" "bimandscan/stable"
    conan create --build="missing" -s compiler.cppstd="$CXX_STD" -o libarchive:shared=False "$@" "libarchive" "bimandscan/stable"
done
